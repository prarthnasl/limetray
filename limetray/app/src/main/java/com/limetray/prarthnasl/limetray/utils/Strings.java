package com.limetray.prarthnasl.limetray.utils;

/**
 * Created by prarthnasl on 8/26/2016.
 */
public class Strings {
    public static final String EMPTY = "";

    public static String nullSafeString(String s) {
        return s == null ? EMPTY : s;
    }

    public static boolean isEmpty(String string) {
        return (string == null || string.trim().equals(EMPTY)) ? true : false;
    }

    public static boolean isNotEmpty(String string) {
        return !isEmpty(string);
    }
}

package com.limetray.prarthnasl.limetray;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.limetray.prarthnasl.limetray.controllers.baseControllers.ServerResponseInterface;
import com.limetray.prarthnasl.limetray.gcm.BackgroundService;
import com.limetray.prarthnasl.limetray.models.Expense;
import com.limetray.prarthnasl.limetray.models.ExpensesResponse;
import com.limetray.prarthnasl.limetray.repos.ServerRepo;
import com.limetray.prarthnasl.limetray.viewHelpers.ExpensesAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements ServerResponseInterface {

    private ArrayList<Expense> expenses = new ArrayList<>();
    private SwipeRefreshLayout refreshLayout;
    private ExpensesAdapter expensesAdapter;
    private RecyclerView recyclerView;

    private GcmNetworkManager gcmNetworkManager;
    private ProgressDialog progress;

    private int mInterval = 10000;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getData(true);
        setRecyclerView();
        setListeners();

        gcmNetworkManager = GcmNetworkManager.getInstance(LimetrayApplication.getContext());
        new BackgroundService(this);
        //startBackgroundDataRefreshService();
        //startTimer();

        mHandler = new Handler();
        startRepeatingTask();
    }

    private void startTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //getData();
            }
        }, 10000);
    }

    private void setRecyclerView() {
        expensesAdapter = new ExpensesAdapter(this, expenses);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(expensesAdapter);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        refreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE);
    }

    public void getData(boolean isLoadFirstTime) {
        if (isLoadFirstTime) {
            progress = ProgressDialog.show(this, "Please Wait",
                    "Loading data....", true);
        }
        ServerRepo repo = new ServerRepo(this, this);
        HashMap<String, String> params = new HashMap<>();
        repo.getInfo(params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sortAsc:
                sortAscAlphabetically();
                return true;
            case R.id.sortDesc:
                sortDescAlphabetically();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sortAscAlphabetically() {
        Collections.sort(expenses, new Comparator<Expense>() {
            @Override
            public int compare(Expense f1, Expense f2) {
                return f1.getCategory().compareToIgnoreCase(f2.getCategory());

            }
        });
        expensesAdapter.notifyDataSetChanged();
    }

    private void sortDescAlphabetically() {

        Collections.sort(expenses, new Comparator<Expense>() {
            @Override
            public int compare(Expense f1, Expense f2) {
                return f2.getCategory().compareToIgnoreCase(f1.getCategory());
            }
        });

        expensesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResponseSuccess(Object response) {
        Log.d("Response", "OnSuccess");
        if (progress.isShowing()) {
            progress.dismiss();
        }
        ExpensesResponse serverResponse = (ExpensesResponse) response;
        expenses.clear();
        for (Expense expense : serverResponse.getExpenses()) {
            if (expense != null && !expense.getCategory().isEmpty()) {
                expenses.add(expense);
            }
        }
        expensesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResponseFailure(Object response) {
        if (progress.isShowing()) {
            progress.dismiss();
        }
        Toast.makeText(LimetrayApplication.getContext(), "There was an error. Please Try Again Later.", Toast.LENGTH_SHORT).show();
    }

    private void setListeners() {

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                expenses.clear();
                getData(true);
                refreshLayout.setRefreshing(false);
            }
        });
    }

    private void startBackgroundDataRefreshService() {
        long periodSecs = 30L;
        long flexSecs = 15L;

        PeriodicTask periodicTask = new PeriodicTask.Builder()
                .setService(BackgroundService.class)
                .setPeriod(periodSecs)
                .setFlex(flexSecs)
                .setTag("limetray")
                .setPersisted(true)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setRequiresCharging(false)
                .setUpdateCurrent(true)
                .build();

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode == ConnectionResult.SUCCESS) {
            gcmNetworkManager.schedule(periodicTask);
        } else {
            //getData();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                getData(false);
            } finally {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
}

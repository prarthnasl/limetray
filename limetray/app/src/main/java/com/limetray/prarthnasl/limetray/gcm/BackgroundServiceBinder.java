package com.limetray.prarthnasl.limetray.gcm;

import android.os.Binder;

/**
 * Created by prarthnasl on 8/26/2016.
 */
public class BackgroundServiceBinder extends Binder {
    BackgroundService service;

    public BackgroundServiceBinder(BackgroundService service) {
        this.service = service;
    }

    public BackgroundService GetBackgroundService() {
        return service;
    }
}
package com.limetray.prarthnasl.limetray.viewHelpers;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.limetray.prarthnasl.limetray.MainActivity;
import com.limetray.prarthnasl.limetray.R;
import com.limetray.prarthnasl.limetray.models.Expense;

import java.util.ArrayList;

/**
 * Created by prarthnasl on 8/25/2016.
 */
public class ExpensesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private MainActivity activity;

    private ArrayList<Expense> expenses;

    private View itemLayoutView;

    public void setExpenses(ArrayList<Expense> expenses) {
        this.expenses = expenses;
    }

    public ExpensesAdapter(Activity activity, ArrayList<Expense> expenses) {
        this.expenses = expenses;
        this.activity = (MainActivity) activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expense_item, parent, false);
        return new ExpenseViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (expenses.get(position) != null) {
            ((ExpenseViewHolder) viewHolder).setup(position, expenses, expenses.get(position), activity);
        }
        itemLayoutView.setVisibility(expenses.get(position) == null ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

}


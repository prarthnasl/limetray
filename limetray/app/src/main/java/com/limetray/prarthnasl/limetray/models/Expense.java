package com.limetray.prarthnasl.limetray.models;

/**
 * Created by prarthnasl on 8/25/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.limetray.prarthnasl.limetray.utils.Strings;

public class Expense {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;

    @SerializedName("state")
    @Expose
    private String state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return Strings.nullSafeString(amount);
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return Strings.nullSafeString(category);
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public String getState() {
        return Strings.nullSafeString(state);
    }

    public void setState(String state) {
        this.state = state;
    }
}
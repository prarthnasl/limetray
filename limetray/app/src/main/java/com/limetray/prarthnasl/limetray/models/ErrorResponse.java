package com.limetray.prarthnasl.limetray.models;

import com.google.gson.annotations.Expose;
import com.limetray.prarthnasl.limetray.utils.Strings;

/**
 * Created by prarthnasl on 8/24/2016.
 */
public class ErrorResponse {

    @Expose
    private String status;

    @Expose
    private String message;

    @Expose
    private Validations validations;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return Strings.nullSafeString(message);
    }

    public void setMessage(String messages) {
        this.message = messages;
    }

    public Validations getValidations() {
        return validations == null ? new Validations() : validations;
    }

    public void setValidations(Validations validations) {
        this.validations = validations;
    }
}


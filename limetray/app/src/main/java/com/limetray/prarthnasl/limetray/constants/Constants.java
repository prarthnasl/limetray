package com.limetray.prarthnasl.limetray.constants;

public class Constants {

    public static boolean TESTING                                       = false;
    public static boolean DEBUG                                         = false;

    // URLs
    public static final String ABSOLUTE_BASE_URL_STAGING                = "https://jsonblob.com";
    public static final String ABSOLUTE_BASE_URL_PRODUCTION             = "https://jsonblob.com";
    public static final String BASE_URL_STAGING                         = ABSOLUTE_BASE_URL_STAGING + "/api/jsonBlob" + "57c09c08e4b0dc55a4f0f4e4";
    public static final String BASE_URL_PRODUCTION                      = "https://jsonblob.com/api/jsonBlob/57c09c08e4b0dc55a4f0f4e4";

    // ID
    public static final String EXPENSES_ID                              = "57bf2a88e4b0dc55a4f0a7b6";
}
package com.limetray.prarthnasl.limetray.gcm;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.limetray.prarthnasl.limetray.MainActivity;

/**
 * Created by prarthnasl on 8/26/2016.
 */
public class BackgroundService extends GcmTaskService {

    MainActivity activity;

    public BackgroundService() {
    }

    public BackgroundService(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public int onRunTask(TaskParams params) {

        Handler handler = new Handler(Looper.getMainLooper());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("Background Service", "Background Task Succeeded");
                activity = new MainActivity();
                //activity.getData();
            }
        }, 100);

        return GcmNetworkManager.RESULT_SUCCESS;
    }
}

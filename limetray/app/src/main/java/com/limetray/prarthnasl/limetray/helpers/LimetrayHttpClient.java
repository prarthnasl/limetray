package com.limetray.prarthnasl.limetray.helpers;

/**
 * Created by prarthnasl on 8/26/2016.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.limetray.prarthnasl.limetray.LimetrayApplication;
import com.limetray.prarthnasl.limetray.constants.Constants;
import com.limetray.prarthnasl.limetray.controllers.baseControllers.ServerResponseListener;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


public class LimetrayHttpClient {

    public enum RequestType {
        GET,
        PUT,
        GET_FULL_URL;
    }

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static AsyncHttpClient getClient() {
        return client;
    }

    public void request(RequestType requestType, String relativeUrl, HashMap<String, String> params, final TypeToken responseObjectType, final ServerResponseListener serverResponseListener) {

        if (!isNetworkConnected()) {
            Toast.makeText(LimetrayApplication.getContext(), "Seems like you are offline", Toast.LENGTH_SHORT).show();
        } else {
            StringRequest stringRequest = new StringRequest(Constants.BASE_URL_PRODUCTION,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            showJSON(response, serverResponseListener, responseObjectType);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(LimetrayApplication.getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(LimetrayApplication.getContext());
            requestQueue.add(stringRequest);
        }
    }

    private void showJSON(String response, ServerResponseListener serverResponseListener, TypeToken responseObjectType) {
        if (response != null) {
            Gson gson = new GsonBuilder().create();
            JsonReader reader = new JsonReader(new StringReader(response));

            try {
                Object responseObject = gson.fromJson(reader, responseObjectType.getType());
                serverResponseListener.handleResponse(responseObject);
            } catch (JsonSyntaxException jse) {
                jse.printStackTrace();
                serverResponseListener.handleResponse(null);
                return;
            } catch (Exception e) {
                e.printStackTrace();

                serverResponseListener.handleResponse(null);
            }
        } else {
            serverResponseListener.handleResponse(null);
        }
    }

    public void requestPut(String baseUrl, String json, final TypeToken responseObjectType, final ServerResponseListener serverResponseListener) {
        if (!isNetworkConnected()) {
            Toast.makeText(LimetrayApplication.getContext(), "Seems like you are offline", Toast.LENGTH_SHORT).show();
        } else {
            RequestQueue queue = Volley.newRequestQueue(LimetrayApplication.getContext());

            try {
                String jsonNew = "{\"expenses\":" + json + "}";
                //final JSONArray jsonArray = new JSONArray(json);

                final JSONObject jsonObject = new JSONObject(jsonNew);


                JsonObjectRequest putRequest = new JsonObjectRequest(Request.Method.PUT, Constants.BASE_URL_PRODUCTION, jsonObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // response
                                Log.d("Response", response.toString());
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Toast.makeText(LimetrayApplication.getContext(), "There was an error processing your request. Please Try Again.", Toast.LENGTH_SHORT).show();
                                Log.d("Error.Response", error.toString());
                            }
                        }
                ) {

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }

                    @Override
                    public byte[] getBody() {

                        try {
                            return jsonObject.toString().getBytes("UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };

                queue.add(putRequest);
            } catch (JSONException e) {
                // handle exception
                e.printStackTrace();
            }
        }
    }

    private RequestParams createRequestParams(HashMap<String, String> params) {
        RequestParams requestParams = new RequestParams();

        for (String key : params.keySet()) {
            String value = params.get(key);
            requestParams.add(key, value);
        }

        return requestParams;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) LimetrayApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    class TextHttpResponseHandler extends com.loopj.android.http.TextHttpResponseHandler {
        private TypeToken responseObjectType;
        private ServerResponseListener serverResponseListener;

        public TextHttpResponseHandler(TypeToken responseObjectType, ServerResponseListener serverResponseListener) {
            super();
            this.responseObjectType = responseObjectType;
            this.serverResponseListener = serverResponseListener;
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String response) {

            if (responseObjectType == null) {
                serverResponseListener.handleResponse(response);
            } else {
                parseResponseAndCallServerResponseHandler(response, responseObjectType, serverResponseListener);
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String response, Throwable throwable) {
            serverResponseListener.handleResponse(null);
        }
    }

    public void parseResponseAndCallServerResponseHandler(String response, TypeToken responseObjectType, ServerResponseListener serverResponseListener) {
        if (response != null) {
            Gson gson = new GsonBuilder().create();
            JsonReader reader = new JsonReader(new StringReader(response));

            try {
                Object responseObject = gson.fromJson(reader, responseObjectType.getType());
                serverResponseListener.handleResponse(responseObject);
            } catch (JsonSyntaxException jse) {
                jse.printStackTrace();
                serverResponseListener.handleResponse(null);
                return;
            } catch (Exception e) {
                e.printStackTrace();

                serverResponseListener.handleResponse(null);
            }
        } else {
            serverResponseListener.handleResponse(null);
        }
    }
}

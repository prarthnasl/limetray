package com.limetray.prarthnasl.limetray.utils;

/**
 * Created by prarthnasl on 8/26/2016.
 */
public class LocalError {
    public static final int SC_NO_NETWORK_CONNECTION = 1023;
    public static final int SC_JSON_SYNTAX_EXCEPTION = 1024;
    public static final int SC_GENERAL_EXCEPTION = 1025;
    public static final int SC_SERVER_RESPONSE_IS_NULL = 1026;
    public static final int SC_ISSUE_IN_FORM_DATA = 1027;

    public int statusCode;
    public String errorMessage;

    public LocalError(int statusCode, String errorMessage) {
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
    }
}

package com.limetray.prarthnasl.limetray.models;

/**
 * Created by prarthnasl on 8/25/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ExpensesResponse {

    @SerializedName("expenses")
    @Expose
    private ArrayList<Expense> expenses = new ArrayList<Expense>();

    public ArrayList<Expense> getExpenses() {
        return expenses == null ? new ArrayList<Expense>() : expenses;
    }

    public void setExpenses(ArrayList<Expense> expenses) {
        this.expenses = expenses;
    }
}
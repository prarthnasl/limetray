package com.limetray.prarthnasl.limetray.models;

import com.google.gson.annotations.Expose;

/**
 * Created by prarthnasl on 8/24/2016.
 */
public class GeneralResponse {
    @Expose
    private boolean success;

    @Expose
    private ErrorResponse errors;

    public GeneralResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ErrorResponse getErrors() {
        return errors == null ? new ErrorResponse() : errors;
    }

    public void setErrors(ErrorResponse errors) {
        this.errors = errors;
    }
}


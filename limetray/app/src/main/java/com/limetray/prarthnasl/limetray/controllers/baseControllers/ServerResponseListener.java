package com.limetray.prarthnasl.limetray.controllers.baseControllers;

/**
 * Created by prarthnasl on 8/26/2016.
 */
public interface ServerResponseListener {
    void handleResponse(Object responseObject);
}

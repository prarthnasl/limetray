package com.limetray.prarthnasl.limetray.repos;

import android.app.Activity;

import com.google.gson.reflect.TypeToken;
import com.limetray.prarthnasl.limetray.constants.Constants;
import com.limetray.prarthnasl.limetray.controllers.baseControllers.ServerResponseInterface;
import com.limetray.prarthnasl.limetray.controllers.baseControllers.ServerResponseListener;
import com.limetray.prarthnasl.limetray.helpers.LimetrayHttpClient;
import com.limetray.prarthnasl.limetray.models.ExpensesResponse;

import org.apache.http.client.params.ClientPNames;

import java.util.HashMap;

/**
 * Created by prarthnasl on 8/26/2016.
 */
public class ServerRepo {

    Activity activity;
    ServerResponseInterface serverResponseInterface;
    LimetrayHttpClient httpClient;

    public ServerRepo(Activity activity, ServerResponseInterface serverResponseInterface) {
        this.activity = activity;
        this.serverResponseInterface = serverResponseInterface;
        httpClient = new LimetrayHttpClient();
    }

    public void getInfo(HashMap<String, String> params) {
        httpClient.getClient().getHttpClient().getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
        httpClient.request(
                LimetrayHttpClient.RequestType.GET,
                Constants.BASE_URL_PRODUCTION,
                params,
                TypeToken.get(ExpensesResponse.class),
                serverResponseListener);
    }

    public void sendInfo(String json) {
        httpClient.requestPut(
                Constants.BASE_URL_PRODUCTION,
                json,
                TypeToken.get(ExpensesResponse.class),
                serverResponseListener);
    }

    private ServerResponseListener serverResponseListener = new ServerResponseListener() {

        @Override
        public void handleResponse(Object response) {
            if (activity != null) {
                ExpensesResponse serverResponse = (ExpensesResponse) response;
                if (serverResponse != null && serverResponse.getExpenses().size() > 0) {
                    serverResponseInterface.onResponseSuccess(response);
                } else {
                    serverResponseInterface.onResponseFailure(response);
                }
            }
        }
    };
}

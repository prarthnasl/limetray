package com.limetray.prarthnasl.limetray;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by prarthnasl on 8/26/2016.
 */

public class LimetrayApplication extends Application {

    private static Context applicationContext;

    public static final String TAG = LimetrayApplication.class
            .getSimpleName();

    private RequestQueue mRequestQueue;

    private static LimetrayApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("LimeTray", "Application onCreate");

        applicationContext = this;
        mInstance = this;
    }

    public static Context getContext() {
        return applicationContext;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            //mRequestQueue = Volley.newRequestQueue(getApplicationContext());
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(URL url) throws IOException {
                    HttpURLConnection connection = super.createConnection(url);
                    connection.setInstanceFollowRedirects(true);

                    return connection;
                }
            });
        }
        return mRequestQueue;
    }

    public static synchronized LimetrayApplication getInstance() {
        return mInstance;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}

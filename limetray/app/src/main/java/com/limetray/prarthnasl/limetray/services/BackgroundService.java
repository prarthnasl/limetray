package com.limetray.prarthnasl.limetray.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by prarthnasl on 8/27/2016.
 */
public class BackgroundService extends Service {


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        BroadcastReceiver appRunning = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String topActivity = intent.getExtras().getString("top_activity");

                Log.i("TopActivity", topActivity);
            }
        };

        registerReceiver(appRunning, new IntentFilter("APP_RUNNING"));

        Timer TIMER = new Timer(true);
        TIMER.scheduleAtFixedRate(new BackgroundTimerTask(), 2000, 2000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        // TODO Auto-generated method stub
        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 100, restartServicePI);
    }

    private class BackgroundTimerTask extends TimerTask {

        @Override
        public void run() {
            //Intent i = new Intent(BackgroundService.this, MeeshoAccessibilityService.class);
            //startService(i);
        }
    }
}
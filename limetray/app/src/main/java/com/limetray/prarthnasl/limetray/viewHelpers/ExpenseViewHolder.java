package com.limetray.prarthnasl.limetray.viewHelpers;

import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.limetray.prarthnasl.limetray.MainActivity;
import com.limetray.prarthnasl.limetray.R;
import com.limetray.prarthnasl.limetray.models.Expense;
import com.limetray.prarthnasl.limetray.repos.ServerRepo;
import com.limetray.prarthnasl.limetray.utils.Strings;

import java.util.ArrayList;

/**
 * Created by prarthnasl on 8/26/2016.
 */
public class ExpenseViewHolder extends RecyclerView.ViewHolder {

    private MainActivity activity;
    private Expense expense;
    private ArrayList<Expense> expenses = new ArrayList<>();

    private TextView expenseCategory;
    private TextView expenseDescription;
    private TextView expenseAmount;

    private ImageView expenseImage;

    private CardView cardView;

    private LinearLayout verifiedLayout;
    private LinearLayout unverifiedLayout;
    private LinearLayout fraudLayout;

    private TextView verifiedText;
    private TextView unverifiedText;
    private TextView fraudText;

    private int position;
    private String rupeeSymbol = Strings.EMPTY;

    public ExpenseViewHolder(View itemLayoutView) {
        super(itemLayoutView);

        cardView = (CardView) itemLayoutView.findViewById(R.id.card_view);

        expenseImage = (ImageView) itemLayoutView.findViewById(R.id.image);
        expenseCategory = (TextView) itemLayoutView.findViewById(R.id.name);
        expenseDescription = (TextView) itemLayoutView.findViewById(R.id.description);
        expenseAmount = (TextView) itemLayoutView.findViewById(R.id.amount);

        verifiedLayout = (LinearLayout) itemLayoutView.findViewById(R.id.verified_layout);
        unverifiedLayout = (LinearLayout) itemLayoutView.findViewById(R.id.unverified_layout);
        fraudLayout = (LinearLayout) itemLayoutView.findViewById(R.id.fraud_layout);

        verifiedText = (TextView) itemLayoutView.findViewById(R.id.verified_text);
        unverifiedText = (TextView) itemLayoutView.findViewById(R.id.unverified_text);
        fraudText = (TextView) itemLayoutView.findViewById(R.id.fraud_text);
    }

    public void setup(int position, ArrayList<Expense> expenses, final Expense expense, final MainActivity activity) {

        this.activity = activity;
        this.expense = expense;
        this.expenses = expenses;
        this.position = position;

        rupeeSymbol = activity.getString(R.string.rupees_symbol);
        cardView.setVisibility(expense == null ? View.GONE : View.VISIBLE);

        expenseCategory.setText(expense.getCategory());
        expenseDescription.setText(expense.getDescription());
        String amt = rupeeSymbol + expense.getAmount();
        expenseAmount.setText(amt);

        switch (expense.getState()) {
            case "VERIFIED":
                toggleButtons(true, false, false);
                break;
            case "UNVERIFIED":
                toggleButtons(false, true, false);
                break;
            case "FRAUD":
                toggleButtons(false, false, true);
                break;
            default:
                toggleButtons(false, true, false);
                break;
        }

        setListeners();
    }

    private void setListeners() {

        verifiedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButtons(true, false, false);
                sendData("VERIFIED", expense);
            }
        });

        unverifiedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButtons(false, true, false);
                sendData("UNVERIFIED", expense);
            }
        });

        fraudLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButtons(false, false, false);
                sendData("FRAUD", expense);
            }
        });
    }

    private void toggleButtons(final boolean isVerified, final boolean isUnverified, final boolean isFraud) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                verifiedText.setTextColor(ContextCompat.getColor(activity, isVerified ? R.color.colorAccent : R.color.gray_color));
                unverifiedText.setTextColor(ContextCompat.getColor(activity, isUnverified ? R.color.colorAccent : R.color.gray_color));
                fraudText.setTextColor(ContextCompat.getColor(activity, isFraud ? R.color.colorAccent : R.color.gray_color));
            }
        }, 100);
    }

    private void sendData(String state, Expense expense) {
        expense.setState(state);
        expenses.set(position, expense);

        Gson gson = new Gson();
        String json = gson.toJson(expenses);

        ServerRepo repo = new ServerRepo(activity, activity);
        repo.sendInfo(json);
    }
}


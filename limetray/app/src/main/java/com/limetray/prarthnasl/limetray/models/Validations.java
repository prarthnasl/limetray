package com.limetray.prarthnasl.limetray.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;

/**
 * Created by prarthnasl on 8/24/2016.
 */
public class Validations {
    @Expose
    private ArrayList<String> email;

    public ArrayList<String> getEmail() {
        return email == null ? new ArrayList<String>() : email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }
}